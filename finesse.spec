Name:		finesse
Version:	2.3
Release:	1%{?dist}
Summary:	Numeric simulation for laser interferometers

Group:		Applications/Engineering
License:	GPLv3
URL:		https://git.ligo.org/finesse/
Source0:	https://git.ligo.org/finesse/finesse/-/archive/2.3/finesse-2.3.tar.gz

BuildRequires:	gsl-devel
Requires:	gnuplot

%description
Finesse is a numeric simulation for laser interferometers using the frequency domain and Hermite-Gauss modes.

%prep
%setup -q -n %{name}-2.3
sed -i -e "s/^GIT_VERSION = .*/GIT_VERSION = %{version}/" src/Makefile


%build
./finesse.sh --build-linux
sed -i -e 's;^### location of the GNUPLOT executable :$;&\nGNUCOMMAND "/usr/bin/gnuplot -persist"\nPYTHONCOMMAND "python $s 2>/dev/null &"\n;' kat.ini


%install
mkdir -p $RPM_BUILD_ROOT%{_bindir}
install -p kat $RPM_BUILD_ROOT%{_bindir}/
mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/kat
install -p kat.ini $RPM_BUILD_ROOT%{_sysconfdir}/kat/

%files
%license LICENSES
%doc CHANGES README.md INSTALL
%{_bindir}/kat
%dir %{_sysconfdir}/kat
%config(noreplace) %{_sysconfdir}/kat/kat.ini


%changelog
* Thu Dec 12 2019 Michael Thomas <michael.thomas@LIGO.ORG> 2.3-1
- Update to 2.3

* Wed Oct 30 2019 Michael Thomas <michael.thomas@LIGO.ORG> 2.2.11-1
- Update to 2.2.11

* Sat Oct 26 2019 Michael Thomas <michael.thomas@LIGO.ORG> 2.2-4
- Patch the version into the Makefile

* Thu Sep 12 2019 Michael Thomas <michael.thomas@LIGO.ORG> 2.2-3
- Disable broken test suite

* Mon Sep 9 2019 Michael Thomas <michael.thomas@LIGO.ORG> 2.2-2
- run test suite after building

* Mon Sep 9 2019 Michael Thomas <michael.thomas@LIGO.ORG> 2.2-1
- Initial package
